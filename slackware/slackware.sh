#!/bin/sh
ORIGIN="ftp6.gwdg.de/pub/linux/slackware/"
echo "#universe +++ Starting mirror sync: Slackware ..." |& nc -N 127.0.0.2 2424
/usr/bin/withlock /mnt/mirror/meta/mirror-slackware.lock rsync -hiav \
--delete --delete-after \
--no-o --no-g --safe-links \
--timeout=60 --contimeout=30 \
--exclude-from=/mnt/mirror/meta/slackware-exclusions \
rsync://$ORIGIN \
/mnt/mirror/slackware/ |& nc -N 127.0.0.2 2424
echo "#universe +++ Synced with $ORIGIN." |& nc -N 127.0.0.2 2424
